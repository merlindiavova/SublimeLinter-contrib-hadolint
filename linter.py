from SublimeLinter.lint import Linter

class Hadolint(Linter):
    cmd = ('hadolint', '${args}', '-')
    regex = r'^.*:(?P<line>[0-9]+) (?P<code>\S+) (?P<message>.+)$'
    multiline = False
    defaults = {'selector': 'source.dockerfile'}
